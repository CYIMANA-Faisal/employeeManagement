import models from '../models';

const departmentExist = async (name) => {
    const department = await models.Departmet.findOne({
        where: { 
            depName: name
        } 
    });
    return department;
};
export default departmentExist;