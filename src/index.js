import cors from 'cors';
import express from 'express';
import routes from './routes';
import { returnError } from './middlewares/errorHandler';

const app = express();

const port = process.env.PORT || 3000;

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));

// routes
app.use('/api/v1/', routes);
// Handle the errors
app.use(returnError);

app.listen(port, () => {
console.log(`App is listening at http://localhost:${port}`);
})

export default app;