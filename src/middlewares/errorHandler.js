export const returnError = (err, req, res, next) => {
    console.log()
    res.status(err.status || 500).send({status:err.status, message:err.message, stack: err.stack});
}