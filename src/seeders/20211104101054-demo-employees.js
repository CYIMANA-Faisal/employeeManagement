'use strict';
import {hashPassword} from '../utils/auth'
module.exports = {
  up: async (queryInterface, Sequelize) => {
      await queryInterface.bulkInsert('Employees', [
      {
        id: "d5334ee2-6929-41e2-8a36-507fe6a34dbe",
        name: "CYIMANA",
        nationalID: "1199880000863213",
        code: 'EMP2761',
        phoneNumber: '0780508308',
        email: 'mutuyi2@gmail.com',
        password: hashPassword('password'),
        dateOfBirth: new Date(),
        status: 'ACTIVE',
        position: 'MANAGER',
        departmentId: '4266fbdb-4d84-483c-8590-5ecac60d43b0',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "7e5d8683-28d9-4e33-9d2f-dbc3375c7624",
        name: "TEST EMPLOYEE",
        nationalID: "1199880000863216",
        code: 'EMP9699',
        phoneNumber: '0780508308',
        email: 'faisalcyimana1998@gmail.com',
        password: hashPassword('password'),
        dateOfBirth: new Date(),
        status: 'INACTIVE',
        position: 'DEVELOPER',
        departmentId: 'd5334ee2-6929-41e2-8a36-507fe6a34dbe',
        createdAt: new Date(),
        updatedAt: new Date()
      },

    ], {});

  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Employees', null, {});
  }
};
