'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

      await queryInterface.bulkInsert('Departmets', [
        {
          id: "d5334ee2-6929-41e2-8a36-507fe6a34dbe",
          depName: "accounting",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: "903053db-0e4f-4da0-8234-5384c8e5de0f",
          depName: "it",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: "1ca97c1c-9e95-4526-996c-35ce1ca7ca34",
          depName: "finance",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          id: "4266fbdb-4d84-483c-8590-5ecac60d43b0",
          depName: "administration",
          createdAt: new Date(),
          updatedAt: new Date()
        }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Departmets', null, {});
  }
};
