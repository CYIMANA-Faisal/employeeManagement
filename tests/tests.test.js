import { expect, request, use } from 'chai';
import chaiHttp from 'chai-http';
import app from '../src/index';
import 'dotenv/config';
import {validUserLogin, invalidUserLogin, updateEmployee, suspendEmployee} from './dummyData'

use(chaiHttp);
describe('WELCOME END-POINTS TESTING', () => {
    it('Should get welcome message', async () => {
      const res = await request(app).get('/api/v1');
      expect(res).to.have.status([200]);
      expect(res.type).to.equal('application/json');
      expect(res.body).to.have.property('message');
      expect(res.body).to.have.property('status');
      expect(res.body.message).to.equal('Welcome to employee management API');
      expect(res.body.status).to.equal(200);
    });
});

describe('USER ENDPOINTS TESTING', () => {
    let token = ''
    it('Should login the user', async () => {
        const res = await request(app).post('/api/v1/user/signin').send(validUserLogin);
        token = res.body.data
        expect(res).to.have.status([200]);
        expect(res.type).to.equal('application/json');
        expect(res.body).to.have.property('message');
        expect(res.body).to.have.property('data');
        expect(res.body).to.have.property('status');
        expect(res.body.message).to.equal(`Signed in successfully`);
        expect(res.body.status).to.equal(200);

    });
    it('Should not login the user', async () => {
        const res = await request(app).post('/api/v1/user/signin').send(invalidUserLogin);
        expect(res).to.have.status([401]);
        expect(res.type).to.equal('application/json');
        expect(res.body).to.have.property('message');
        expect(res.body).to.have.property('status');
        expect(res.body.message).to.equal(`Invalid credentials`);
        expect(res.body.status).to.equal(401);
    });
    it('Should update employee', async () => {
        const res = await request(app).put('/api/v1//employee/edit/EMP2761').set('Authorization', `Bearer ${token}`).send(updateEmployee);
        expect(res).to.have.status([200]);
    });
    it('Should not update employee', async () => {
        const res = await request(app).put('/api/v1//employee/edit/EMP2761').send(updateEmployee);
        expect(res).to.have.status([401]);
    });
    it('Should suspend employee', async () => {
        const res = await request(app).put('/api/v1//employee/suspend/EMP2761').set('Authorization', `Bearer ${token}`).send(suspendEmployee);
        expect(res).to.have.status([200]);
    });
    it('Should not suspend employee', async () => {
        const res = await request(app).put('/api/v1//employee/suspend/EMP2761').send(suspendEmployee);
        expect(res).to.have.status([401]);
    });
    it('Should activate employee', async () => {
        const res = await request(app).put('/api/v1//employee/activate/EMP2761').set('Authorization', `Bearer ${token}`).send();
        expect(res).to.have.status([200]);
    });
    it('Should not activate employee', async () => {
        const res = await request(app).put('/api/v1//employee/activate/EMP2761').send();
        expect(res).to.have.status([401]);
    });
    it('Should delete employee', async () => {
        const res = await request(app).delete('/api/v1//employee/EMP2761').set('Authorization', `Bearer ${token}`).send();
        expect(res).to.have.status([200]);
    });
    it('Should not delete employee', async () => {
        const res = await request(app).delete('/api/v1//employee/EMP2761').send();
        expect(res).to.have.status([401]);
    });
});