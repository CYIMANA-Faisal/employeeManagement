# Project Title

Employee management
---
## Requirements

For development, you will only need Node.js, postman to make API calls, and a and postgres database setted up.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v16.13.0

    $ npm --version
    8.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

###
### Postman
- #### Postman installation on Windows, linux, macos
    You can install postman in your computer easily by downloading the installer file from [postman website](https://www.postman.com/), just download the installer for your platform.
- #### Postman collections for this project
    Once you are done installing the postman client localy please download this collection that has pre-populated endpoint[postman collection](https://drive.google.com/file/d/14Obe2cRU9T2zD2VBgJ4hY9-HJS6xV0ln/view?usp=sharing), and make sure you create the postman invironment variable that are required.
    
### Entity Relationship Diagram for this project
![](imgs/ERD2.png)
### Usecase Diagram for this project
![](imgs/newusecase.png)

### installation 
---

### Install

    $ git clone https://gitlab.com/CYIMANA-Faisal/employeeManagement.git
    $ cd employees_Management_API
    $ npm install

## Configure app

Open `the sample_env file from root directory` then create the file named .env populate the created file with all the variable in sample_env.md file then edit it with the required variables settings. You will need:

- A testing db;
- A development db;

Make sure you provided the required environment variables as defined in the sample_env file for the project to run better and smooth.

## Running the project
- #### in development mode
    $ npm run dev
- #### to run the tests
    $ npm run test

## Simple build for production

    $ npm run build